import axios from 'axios';

const API_URL = 'http://localhost:3000/api/users';

export default {
  loginUser(credentials) {
    return axios.post(`${API_URL}/login`, credentials);
  },
  // Add other API calls as required
};
