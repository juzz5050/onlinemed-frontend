import axios from 'axios';

const state = {
    user: JSON.parse(localStorage.getItem('user')) || null,
    token: (JSON.parse(localStorage.getItem('user')) && JSON.parse(localStorage.getItem('user')).accessToken) || null,
  };

const mutations = {
  SET_USER_DATA(state, userData) {
    state.user = {
        name: userData.name,
        email: userData.email,
        role: userData.role,
        // ... other user attributes as necessary
    };
    state.token = userData.accessToken;
    localStorage.setItem('user', JSON.stringify(userData));
    axios.defaults.headers.common['Authorization'] = `Bearer ${userData.accessToken}`;
},

  CLEAR_USER_DATA(state) {
    state.user = null;
    state.token = null;
    localStorage.removeItem('user');
    delete axios.defaults.headers.common['Authorization'];
  }

};

const actions = {
  async login({ commit }, credentials) {
    try {
      const response = await axios.post('http://localhost:3000/api/users/login', credentials);
      commit('SET_USER_DATA', response.data);
    } catch (error) {
      console.error("There was an error logging in:", error);
      throw error; // throwing error so that we can catch it in component
    }
  },
  
  logout({ commit }) {
    commit('CLEAR_USER_DATA');
  },

  loadStoredUser({ commit }) {
    const storedUser = localStorage.getItem('user');
    if (storedUser) {
      const userData = JSON.parse(storedUser);
      commit('SET_USER_DATA', userData);
    }
  }
};

const getters = {
  loggedIn(state) {
    return !!state.user;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
