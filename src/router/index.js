import { createRouter, createWebHistory } from 'vue-router';
import store from '../store'; // Import your Vuex store
import Login from '@/views/Login_page.vue';
import Dashboard from '@/views/Dashboard_page.vue';
import PatientList from '@/components/PatientList.vue';
import DashView from '@/components/DashView.vue';
import NewUser from '@/components/NewUser.vue';
import PatientReport from '@/components/PatientReport.vue';
import ConsultView from '@/components/ConsultView.vue';
import PharmView from '@/components/PharmView.vue';
import LabView from '@/components/LabView.vue';

const routes = [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: { requiresAuth: true },  // This route requires authentication
      children: [
        {
          path: '',
          component: DashView,
        },
        {
          path: 'patients',
          component: PatientList,
          meta: { roles: ['admin', 'doctor','pharmacist', 'lab_technician'] } // only show if role is admin or doctor
        },
        {
          path: 'new-user',
          component: NewUser,
          meta: { roles: ['admin'] } 
        },
        {
          path: 'patient-report/:patientId',
          component: PatientReport,
          props: true,
          meta: { roles: ['admin', 'doctor'] } 
        },
        {
          path: 'consult/:patientId',
          component: ConsultView, 
          props: true,
          meta: { roles: ['admin', 'doctor'] } 
        },
        {
          path: 'pharmview/:patientId',
          component: PharmView, 
          props: true,
          meta: { roles: ['pharmacist'] } 
        },
        {
          path: 'labview/:patientId',
          component: LabView, 
          props: true,
          meta: { roles: ['lab_technician'] } 
        },
        // Add other paths for the sidebar items
      ]
    },
    
    // ... other routes
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
  const user = store.state.auth.user;

  // Check if the route requires authentication
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!user) {
      // Redirect to the login page if user is not authenticated
      next({ name: 'Login' });
      return;
    }

    // Check if the authenticated user has the right role for this route
    if (to.matched.some(record => record.meta.roles && !record.meta.roles.includes(user.role))) {
      next({ name: 'Dashboard' }); // Redirect to the dashboard if they don't have permission
      return;
    }
  }
  
  next();
});

export default router;
