import { createApp } from 'vue';
import App from './App.vue';
import router from './router'; // Make sure your router file is at this path
import { createStore } from 'vuex';
import authModule from './store/modules/auth';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import '@/assets/scss/styles.scss';

const store = createStore({
    modules: {
      auth: authModule
    }
  });

store.dispatch('auth/loadStoredUser');

const app = createApp(App);
app.use(store);
app.use(router);
app.mount('#app');
